# NodeJS - Curso Alura

## Conceitos abordados
- O que é a plataforma Node;
- O que é o npm;
- Como criar um servidor HTTP no Node;
- Iniciar projeto Node com o npm init;
- O que é e como funciona o package.json;
- Instalar pacotes com o npm;
- Módulo express;


