const express = require('express')
const http = require('http')

const app = express()


app.get('/', function(req, res) {
    res.send(
        `
            <html>
                <head>
                    <meta charset="utf-8">
                </head>
                <body>
                    <h1>Casa do Código</h1>
                </body>
            </html>
        `
    );
});

app.get('/livros', function(req, res) {
    res.send(
        `
            <html>
                <head>
                    <meta charset="utf-8">
                </head>
                <body>
                    <h1>Página de Livros</h1>
                </body>
            </html>
        `
    );
});


app.listen(3000, function() {
    console.log(`Servidor rodando na porta 3000`);
})

// const servidor = http.createServer(function (req, res) {

//     let html = '';
//     if (req.url == '/') {
//         html = `
//             <html>
//                 <head>
//                     <meta charset="utf-8">
//                 </head>
//                 <body>
//                     <h1>Casa do Código</h1>
//                 </body>
//             </html>
//         `;
//     } else if (req.url == '/livros') {
//         html = `
//             <html>
//                 <head>
//                     <meta charset="utf-8">
//                 </head>
//                 <body>
//                     <h1>Página de Livros</h1>
//                 </body>
//             </html>
//         `;
//     }

//     res.end(html);
// }).listen(3000, 'localhost');
